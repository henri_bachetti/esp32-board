# ESP32 Prototyping Board

The purpose of this page is to explain how to develop an efficient low-power ESP32 board, powered by a LIPO battery.

The ESP32 is located on a separate breakboard.

The board includes :

 * a LIPO battery
 * a TP4056 charger
 * an RT9080 regulator
 * an FT232RL module
 * 2 x 2N2222 transistors
 * some resistors and capacitors

### ARDUINO

The code is build using ARDUINO IDE 1.8.19.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2022/02/esp32-prototypage-suite.html

